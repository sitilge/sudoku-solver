package fuzzing

import (
	"git.sitilge.id.lv/sitilge/sudoku-solver"
	fuzz "github.com/google/gofuzz"
)

func Fuzz(data []byte) int {
	var input [9][9]uint8
	fuzz.NewFromGoFuzz(data).Fuzz(&input)

	//Create a new matrix.
	m, err := solver.NewMatrix(input)
	if err != nil {
		return 0
	}

	//Solve the puzzle.
	_, err = m.Solve()
	if err != nil {
		//I am interested in finding an unsolvable sudoku.
		panic(err)
	}

	return 1
}
