package solver

import (
	"fmt"
	"log"
	"reflect"
	"testing"
)

var matrices = []struct {
	Rows     [9][9]uint8
	Cols     [9][9]uint8
	Solution Matrix
}{
	{
		//Unsolved easy matrix, many missing digits
		Rows: [9][9]uint8{
			{0, 4, 2, 0, 0, 0, 0, 0, 5},
			{0, 0, 0, 6, 3, 2, 0, 8, 0},
			{0, 8, 0, 0, 4, 0, 2, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 0},
			{7, 1, 5, 0, 6, 8, 3, 4, 0},
			{9, 0, 8, 3, 5, 0, 7, 6, 1},
			{0, 9, 1, 0, 0, 6, 0, 0, 0},
			{0, 0, 0, 0, 2, 0, 1, 9, 0},
			{0, 0, 6, 1, 0, 0, 0, 5, 0},
		},
		Cols: [9][9]uint8{
			{0, 0, 0, 0, 7, 9, 0, 0, 0},
			{4, 0, 8, 0, 1, 0, 9, 0, 0},
			{2, 0, 0, 0, 5, 8, 1, 0, 6},
			{0, 6, 0, 0, 0, 3, 0, 0, 1},
			{0, 3, 4, 0, 6, 5, 0, 2, 0},
			{0, 2, 0, 0, 8, 0, 6, 0, 0},
			{0, 0, 2, 0, 3, 7, 0, 1, 0},
			{0, 8, 0, 0, 4, 6, 0, 9, 5},
			{5, 0, 0, 0, 0, 1, 0, 0, 0},
		},
		Solution: Matrix{
			Rows: [9][9]uint8{
				{3, 4, 2, 8, 9, 1, 6, 7, 5},
				{1, 5, 7, 6, 3, 2, 9, 8, 4},
				{6, 8, 9, 7, 4, 5, 2, 1, 3},
				{4, 6, 3, 9, 1, 7, 5, 2, 8},
				{7, 1, 5, 2, 6, 8, 3, 4, 9},
				{9, 2, 8, 3, 5, 4, 7, 6, 1},
				{5, 9, 1, 4, 7, 6, 8, 3, 2},
				{8, 7, 4, 5, 2, 3, 1, 9, 6},
				{2, 3, 6, 1, 8, 9, 4, 5, 7},
			},
			Cols: [9][9]uint8{
				{3, 1, 6, 4, 7, 9, 5, 8, 2},
				{4, 5, 8, 6, 1, 2, 9, 7, 3},
				{2, 7, 9, 3, 5, 8, 1, 4, 6},
				{8, 6, 7, 9, 2, 3, 4, 5, 1},
				{9, 3, 4, 1, 6, 5, 7, 2, 8},
				{1, 2, 5, 7, 8, 4, 6, 3, 9},
				{6, 9, 2, 5, 3, 7, 8, 1, 4},
				{7, 8, 1, 2, 4, 6, 3, 9, 5},
				{5, 4, 3, 8, 9, 1, 2, 6, 7},
			},
		},
	},
	{
		//Unsolved easy matrix, one missing digit
		Rows: [9][9]uint8{
			{3, 4, 2, 8, 9, 1, 6, 7, 5},
			{1, 5, 7, 6, 3, 2, 9, 8, 4},
			{6, 8, 9, 7, 4, 5, 2, 1, 3},
			{4, 6, 3, 9, 1, 7, 5, 2, 8},
			{7, 1, 5, 2, 6, 8, 3, 4, 9},
			{9, 2, 8, 3, 5, 4, 7, 6, 1},
			{5, 9, 1, 4, 7, 6, 8, 3, 2},
			{8, 7, 4, 5, 2, 3, 1, 9, 6},
			{2, 3, 6, 1, 8, 9, 4, 5, 0},
		},
		Cols: [9][9]uint8{
			{3, 1, 6, 4, 7, 9, 5, 8, 2},
			{4, 5, 8, 6, 1, 2, 9, 7, 3},
			{2, 7, 9, 3, 5, 8, 1, 4, 6},
			{8, 6, 7, 9, 2, 3, 4, 5, 1},
			{9, 3, 4, 1, 6, 5, 7, 2, 8},
			{1, 2, 5, 7, 8, 4, 6, 3, 9},
			{6, 9, 2, 5, 3, 7, 8, 1, 4},
			{7, 8, 1, 2, 4, 6, 3, 9, 5},
			{5, 4, 3, 8, 9, 1, 2, 6, 0},
		},
		Solution: Matrix{
			Rows: [9][9]uint8{
				{3, 4, 2, 8, 9, 1, 6, 7, 5},
				{1, 5, 7, 6, 3, 2, 9, 8, 4},
				{6, 8, 9, 7, 4, 5, 2, 1, 3},
				{4, 6, 3, 9, 1, 7, 5, 2, 8},
				{7, 1, 5, 2, 6, 8, 3, 4, 9},
				{9, 2, 8, 3, 5, 4, 7, 6, 1},
				{5, 9, 1, 4, 7, 6, 8, 3, 2},
				{8, 7, 4, 5, 2, 3, 1, 9, 6},
				{2, 3, 6, 1, 8, 9, 4, 5, 7},
			},
			Cols: [9][9]uint8{
				{3, 1, 6, 4, 7, 9, 5, 8, 2},
				{4, 5, 8, 6, 1, 2, 9, 7, 3},
				{2, 7, 9, 3, 5, 8, 1, 4, 6},
				{8, 6, 7, 9, 2, 3, 4, 5, 1},
				{9, 3, 4, 1, 6, 5, 7, 2, 8},
				{1, 2, 5, 7, 8, 4, 6, 3, 9},
				{6, 9, 2, 5, 3, 7, 8, 1, 4},
				{7, 8, 1, 2, 4, 6, 3, 9, 5},
				{5, 4, 3, 8, 9, 1, 2, 6, 7},
			},
		},
	},
	{
		//Solved easy matrix
		Rows: [9][9]uint8{
			{3, 4, 2, 8, 9, 1, 6, 7, 5},
			{1, 5, 7, 6, 3, 2, 9, 8, 4},
			{6, 8, 9, 7, 4, 5, 2, 1, 3},
			{4, 6, 3, 9, 1, 7, 5, 2, 8},
			{7, 1, 5, 2, 6, 8, 3, 4, 9},
			{9, 2, 8, 3, 5, 4, 7, 6, 1},
			{5, 9, 1, 4, 7, 6, 8, 3, 2},
			{8, 7, 4, 5, 2, 3, 1, 9, 6},
			{2, 3, 6, 1, 8, 9, 4, 5, 7},
		},
		Cols: [9][9]uint8{
			{3, 1, 6, 4, 7, 9, 5, 8, 2},
			{4, 5, 8, 6, 1, 2, 9, 7, 3},
			{2, 7, 9, 3, 5, 8, 1, 4, 6},
			{8, 6, 7, 9, 2, 3, 4, 5, 1},
			{9, 3, 4, 1, 6, 5, 7, 2, 8},
			{1, 2, 5, 7, 8, 4, 6, 3, 9},
			{6, 9, 2, 5, 3, 7, 8, 1, 4},
			{7, 8, 1, 2, 4, 6, 3, 9, 5},
			{5, 4, 3, 8, 9, 1, 2, 6, 7},
		},
		Solution: Matrix{
			Rows: [9][9]uint8{
				{3, 4, 2, 8, 9, 1, 6, 7, 5},
				{1, 5, 7, 6, 3, 2, 9, 8, 4},
				{6, 8, 9, 7, 4, 5, 2, 1, 3},
				{4, 6, 3, 9, 1, 7, 5, 2, 8},
				{7, 1, 5, 2, 6, 8, 3, 4, 9},
				{9, 2, 8, 3, 5, 4, 7, 6, 1},
				{5, 9, 1, 4, 7, 6, 8, 3, 2},
				{8, 7, 4, 5, 2, 3, 1, 9, 6},
				{2, 3, 6, 1, 8, 9, 4, 5, 7},
			},
			Cols: [9][9]uint8{
				{3, 1, 6, 4, 7, 9, 5, 8, 2},
				{4, 5, 8, 6, 1, 2, 9, 7, 3},
				{2, 7, 9, 3, 5, 8, 1, 4, 6},
				{8, 6, 7, 9, 2, 3, 4, 5, 1},
				{9, 3, 4, 1, 6, 5, 7, 2, 8},
				{1, 2, 5, 7, 8, 4, 6, 3, 9},
				{6, 9, 2, 5, 3, 7, 8, 1, 4},
				{7, 8, 1, 2, 4, 6, 3, 9, 5},
				{5, 4, 3, 8, 9, 1, 2, 6, 7},
			},
		},
	},
	{
		//Unsolved hard matrix, many missing digits
		Rows: [9][9]uint8{
			{5, 0, 0, 0, 1, 0, 0, 0, 0},
			{0, 9, 0, 4, 0, 0, 6, 3, 0},
			{3, 0, 0, 0, 0, 0, 9, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 7},
			{0, 0, 1, 6, 0, 0, 0, 0, 0},
			{0, 0, 0, 1, 9, 0, 2, 0, 5},
			{7, 0, 0, 0, 0, 0, 0, 0, 0},
			{0, 8, 0, 9, 0, 4, 1, 0, 0},
			{2, 0, 0, 0, 3, 0, 0, 4, 0},
		},
		Cols: [9][9]uint8{
			{5, 0, 3, 0, 0, 0, 7, 0, 2},
			{0, 9, 0, 0, 0, 0, 0, 8, 0},
			{0, 0, 0, 0, 1, 0, 0, 0, 0},
			{0, 4, 0, 0, 6, 1, 0, 9, 0},
			{1, 0, 0, 0, 0, 9, 0, 0, 3},
			{0, 0, 0, 0, 0, 0, 0, 4, 0},
			{0, 6, 9, 0, 0, 2, 0, 1, 0},
			{0, 3, 0, 0, 0, 0, 0, 0, 4},
			{0, 0, 0, 7, 0, 5, 0, 0, 0},
		},
		Solution: Matrix{
			Rows: [9][9]uint8{
				{5, 6, 8, 3, 1, 9, 4, 7, 2},
				{1, 9, 7, 4, 5, 2, 6, 3, 8},
				{3, 4, 2, 7, 8, 6, 9, 5, 1},
				{9, 2, 6, 5, 4, 3, 8, 1, 7},
				{8, 5, 1, 6, 2, 7, 3, 9, 4},
				{4, 7, 3, 1, 9, 8, 2, 6, 5},
				{7, 3, 4, 2, 6, 1, 5, 8, 9},
				{6, 8, 5, 9, 7, 4, 1, 2, 3},
				{2, 1, 9, 8, 3, 5, 7, 4, 6},
			},
			Cols: [9][9]uint8{
				{5, 1, 3, 9, 8, 4, 7, 6, 2},
				{6, 9, 4, 2, 5, 7, 3, 8, 1},
				{8, 7, 2, 6, 1, 3, 4, 5, 9},
				{3, 4, 7, 5, 6, 1, 2, 9, 8},
				{1, 5, 8, 4, 2, 9, 6, 7, 3},
				{9, 2, 6, 3, 7, 8, 1, 4, 5},
				{4, 6, 9, 8, 3, 2, 5, 1, 7},
				{7, 3, 5, 1, 9, 6, 8, 2, 4},
				{2, 8, 1, 7, 4, 5, 9, 3, 6},
			},
		},
	},
}

func TestNewMatrix(t *testing.T) {
	type args struct {
		input [9][9]uint8
	}
	tests := []struct {
		name    string
		args    args
		want    *Matrix
		wantErr bool
	}{
		{
			name: "Valid matrix",
			args: args{
				input: matrices[0].Rows,
			},
			want: &Matrix{
				Rows: matrices[0].Rows,
				Cols: matrices[0].Cols,
			},
			wantErr: false,
		},
		{
			name: "Invalid matrix, invalid digit",
			args: args{
				input: [9][9]uint8{
					{10, 4, 2, 0, 0, 0, 0, 0, 5},
					{0, 0, 0, 6, 3, 2, 0, 8, 0},
					{0, 8, 0, 0, 4, 0, 2, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0},
					{7, 1, 5, 0, 6, 8, 3, 4, 0},
					{9, 0, 8, 3, 5, 0, 7, 6, 1},
					{0, 9, 1, 0, 0, 6, 0, 0, 0},
					{0, 0, 0, 0, 2, 0, 1, 9, 0},
					{0, 0, 6, 1, 0, 0, 0, 5, 0},
				},
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewMatrix(tt.args.input)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewMatrix() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewMatrix() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewCoords(t *testing.T) {
	type args struct {
		x uint8
		y uint8
	}
	tests := []struct {
		name    string
		args    args
		want    *Coords
		wantErr bool
	}{
		{
			name: "Valid coords",
			args: args{
				x: 1,
				y: 1,
			},
			want: &Coords{
				X: 1,
				Y: 1,
			},
			wantErr: false,
		},
		{
			name: "Invalid X coords",
			args: args{
				x: 9,
				y: 1,
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "Invalid Y coords",
			args: args{
				x: 1,
				y: 9,
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewCoords(tt.args.x, tt.args.y)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewCoords() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewCoords() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMatrix_Valid(t *testing.T) {
	type fields struct {
		Rows [9][9]uint8
		Cols [9][9]uint8
	}
	type args struct {
		digit  uint8
		coords *Coords
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "Valid digit",
			fields: fields{
				Rows: matrices[0].Rows,
				Cols: matrices[0].Cols,
			},
			args: args{
				digit: 1,
				coords: &Coords{
					X: 0,
					Y: 0,
				},
			},
			want: true,
		},
		{
			name: "Invalid digit 0",
			fields: fields{
				Rows: matrices[0].Rows,
				Cols: matrices[0].Cols,
			},
			args: args{
				digit: 0,
				coords: &Coords{
					X: 0,
					Y: 0,
				},
			},
			want: false,
		},
		{
			name: "Invalid digit 10",
			fields: fields{
				Rows: matrices[0].Rows,
				Cols: matrices[0].Cols,
			},
			args: args{
				digit: 10,
				coords: &Coords{
					X: 0,
					Y: 0,
				},
			},
			want: false,
		},
		{
			name: "Invalid digit, already exists",
			fields: fields{
				Rows: matrices[1].Rows,
				Cols: matrices[1].Cols,
			},
			args: args{
				digit: 5,
				coords: &Coords{
					X: 5,
					Y: 5,
				},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &Matrix{
				Rows: tt.fields.Rows,
				Cols: tt.fields.Cols,
			}
			if got := m.Valid(tt.args.digit, tt.args.coords); got != tt.want {
				t.Errorf("Matrix.Valid() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMatrix_LocalizeSquare(t *testing.T) {
	type fields struct {
		Rows [9][9]uint8
		Cols [9][9]uint8
	}
	type args struct {
		coords *Coords
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *Square
	}{
		{
			name: "Valid square localization",
			fields: fields{
				Rows: matrices[0].Rows,
				Cols: matrices[0].Cols,
			},
			args: args{
				coords: &Coords{
					X: 0,
					Y: 0,
				},
			},
			want: &Square{
				Rows: [3][3]uint8{
					{0, 4, 2},
					{0, 0, 0},
					{0, 8, 0},
				},
				Cols: [3][3]uint8{
					{0, 0, 0},
					{4, 0, 8},
					{2, 0, 0},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &Matrix{
				Rows: tt.fields.Rows,
				Cols: tt.fields.Cols,
			}
			if got := m.LocalizeSquare(tt.args.coords); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Matrix.LocalizeSquare() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMatrix_Solve(t *testing.T) {
	type fields struct {
		Rows [9][9]uint8
		Cols [9][9]uint8
	}
	tests := []struct {
		name    string
		fields  fields
		want    *Matrix
		wantErr bool
	}{
		{
			name: "Valid matrix, one solution",
			fields: fields{
				Rows: matrices[0].Rows,
				Cols: matrices[0].Cols,
			},
			want: &Matrix{
				Rows: matrices[0].Solution.Rows,
				Cols: matrices[0].Solution.Cols,
			},
			wantErr: false,
		},
		{
			name: "Valid matrix, one solution, long calculation",
			fields: fields{
				Rows: matrices[3].Rows,
				Cols: matrices[3].Cols,
			},
			want: &Matrix{
				Rows: matrices[3].Solution.Rows,
				Cols: matrices[3].Solution.Cols,
			},
			wantErr: false,
		},
		{
			name: "Valid matrix, one digit missing matrix, one solution",
			fields: fields{
				Rows: matrices[2].Rows,
				Cols: matrices[2].Cols,
			},
			want: &Matrix{
				Rows: matrices[2].Solution.Rows,
				Cols: matrices[2].Solution.Cols,
			},
			wantErr: false,
		},
		{
			name: "Valid matrix, already solved",
			fields: fields{
				Rows: matrices[2].Rows,
				Cols: matrices[2].Cols,
			},
			want: &Matrix{
				Rows: matrices[2].Solution.Rows,
				Cols: matrices[2].Solution.Cols,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &Matrix{
				Rows: tt.fields.Rows,
				Cols: tt.fields.Cols,
			}
			got, err := m.Solve()
			if (err != nil) != tt.wantErr {
				t.Errorf("Matrix.Solve() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Matrix.Solve() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMatrix_solve(t *testing.T) {
	type fields struct {
		Rows [9][9]uint8
		Cols [9][9]uint8
	}
	tests := []struct {
		name    string
		fields  fields
		want    bool
		wantErr bool
	}{
		{
			name: "Valid matrix, solve level 0",
			fields: fields{
				Rows: matrices[0].Rows,
				Cols: matrices[0].Cols,
			},
			want:    true,
			wantErr: false,
		},
		{
			name: "Invalid matrix, solve level 0",
			fields: fields{
				Rows: [9][9]uint8{
					{7, 4, 2, 0, 0, 0, 0, 0, 5},
					{0, 0, 0, 6, 3, 2, 0, 8, 0},
					{0, 8, 0, 0, 4, 0, 2, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0},
					{7, 1, 5, 0, 6, 8, 3, 4, 0},
					{9, 0, 8, 3, 5, 0, 7, 6, 1},
					{0, 9, 1, 0, 0, 6, 0, 0, 0},
					{0, 0, 0, 0, 2, 0, 1, 9, 0},
					{0, 0, 6, 1, 0, 0, 0, 5, 0},
				},
				Cols: [9][9]uint8{
					{7, 0, 0, 0, 7, 9, 0, 0, 0},
					{4, 0, 8, 0, 1, 0, 9, 0, 0},
					{2, 0, 0, 0, 5, 8, 1, 0, 6},
					{0, 6, 0, 0, 0, 3, 0, 0, 1},
					{0, 3, 4, 0, 6, 5, 0, 2, 0},
					{0, 2, 0, 0, 8, 0, 6, 0, 0},
					{0, 0, 2, 0, 3, 7, 0, 1, 0},
					{0, 8, 0, 0, 4, 6, 0, 9, 5},
					{5, 0, 0, 0, 0, 1, 0, 0, 0},
				},
			},
			want:    false,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &Matrix{
				Rows: tt.fields.Rows,
				Cols: tt.fields.Cols,
			}
			if got := m.solve(); got != tt.want {
				t.Errorf("Matrix.solve() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMatrix_String(t *testing.T) {
	type fields struct {
		Rows [9][9]uint8
		Cols [9][9]uint8
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "Valid string",
			fields: fields{
				Rows: matrices[0].Rows,
				Cols: matrices[0].Cols,
			},
			want: `
+---+---+---+
| 42|   |  5|
|   |632| 8 |
| 8 | 4 |2  |
+---+---+---+
|   |   |   |
|715| 68|34 |
|9 8|35 |761|
+---+---+---+
| 91|  6|   |
|   | 2 |19 |
|  6|1  | 5 |
+---+---+---+
`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &Matrix{
				Rows: tt.fields.Rows,
				Cols: tt.fields.Cols,
			}
			if got := m.String(); got != tt.want {
				t.Errorf("Matrix.String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSquare_String(t *testing.T) {
	type fields struct {
		Rows [3][3]uint8
		Cols [3][3]uint8
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "Valid string",
			fields: fields{
				Rows: [3][3]uint8{
					{1, 2, 3},
					{1, 2, 3},
					{1, 2, 0},
				},
				Cols: [3][3]uint8{
					{1, 1, 1},
					{2, 2, 2},
					{3, 3, 0},
				},
			},
			want: `
+---+
|123|
|123|
|12 |
+---+
`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &Square{
				Rows: tt.fields.Rows,
				Cols: tt.fields.Cols,
			}
			if got := s.String(); got != tt.want {
				t.Errorf("Square.String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMatrix_Hash(t *testing.T) {
	type fields struct {
		Rows [9][9]uint8
		Cols [9][9]uint8
	}
	tests := []struct {
		name    string
		fields  fields
		want    string
		wantErr bool
	}{
		{
			name: "Valid hash",
			fields: fields{
				Rows: matrices[3].Rows,
				Cols: matrices[3].Cols,
			},
			want:    "6f6b3ab740a9c16ba651d9fafb51dbc65c0883196de561b5aba63a1ac517d469",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &Matrix{
				Rows: tt.fields.Rows,
				Cols: tt.fields.Cols,
			}
			got, err := m.Hash()
			if (err != nil) != tt.wantErr {
				t.Errorf("Matrix.Hash() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Matrix.Hash() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMatrix_Solved(t *testing.T) {
	type fields struct {
		Rows [9][9]uint8
		Cols [9][9]uint8
	}
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			name: "Unsolved matrix",
			fields: fields{
				Rows: matrices[0].Rows,
				Cols: matrices[0].Cols,
			},
			want: false,
		},
		{
			name: "Solved matrix",
			fields: fields{
				Rows: matrices[2].Rows,
				Cols: matrices[2].Cols,
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &Matrix{
				Rows: tt.fields.Rows,
				Cols: tt.fields.Cols,
			}
			if got := m.Solved(); got != tt.want {
				t.Errorf("Matrix.Solved() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMatrix_Copy(t *testing.T) {
	type fields struct {
		Rows [9][9]uint8
		Cols [9][9]uint8
	}
	tests := []struct {
		name   string
		fields fields
		want   *Matrix
	}{
		{
			name: "Valid matrix",
			fields: fields{
				Rows: matrices[0].Rows,
				Cols: matrices[0].Cols,
			},
			want: &Matrix{
				Rows: matrices[0].Rows,
				Cols: matrices[0].Cols,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &Matrix{
				Rows: tt.fields.Rows,
				Cols: tt.fields.Cols,
			}
			if got := m.Copy(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Matrix.Copy() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMatrix_Transpose(t *testing.T) {
	type fields struct {
		Rows [9][9]uint8
		Cols [9][9]uint8
	}
	tests := []struct {
		name   string
		fields fields
		want   *Matrix
	}{
		{
			name: "Valid matrix",
			fields: fields{
				Rows: matrices[0].Rows,
				Cols: matrices[0].Cols,
			},
			want: &Matrix{
				Rows: matrices[0].Cols,
				Cols: matrices[0].Rows,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &Matrix{
				Rows: tt.fields.Rows,
				Cols: tt.fields.Cols,
			}
			if got := m.Transpose(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Matrix.Transpose() = %v, want %v", got, tt.want)
			}
		})
	}
}

func BenchmarkMatrix_Solve(b *testing.B) {
	for i, matrix := range matrices {
		b.Run(fmt.Sprintf("matrix %v", i), func(b *testing.B) {
			for n := 0; n < b.N; n++ {
				m, err := NewMatrix(matrix.Rows)
				if err != nil {
					log.Fatal(err)
				}

				_, err = m.Solve()
				if err != nil {
					log.Fatal(err)
				}
			}
		})
	}
}
