//Package solver provides functionality for solving sudoku puzzles.
package solver

import (
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"fmt"
	"strconv"
)

//Matrix holds all puzzle rows and cols.
type Matrix struct {
	Rows [9][9]uint8
	Cols [9][9]uint8
}

//Square holds rows and cols of a single square.
type Square struct {
	Rows [3][3]uint8
	Cols [3][3]uint8
}

//Coords represent the X and Y values in matrix or square.
type Coords struct {
	X uint8
	Y uint8
}

//NewMatrix accepts the two-dimensional array of rows and outputs a new matrix.
func NewMatrix(input [9][9]uint8) (*Matrix, error) {
	m := &Matrix{}

	//It is proved that the min number of clues is 17 for sudoku to have
	//a unique solution.
	var clues uint8 = 81

	//Check if the input matrix is valid.
	for i, row := range input {
		for j, digit := range row {
			if digit > 9 {
				return nil, fmt.Errorf("invalid digit: %v", digit)
			}

			if digit == 0 {
				clues--

				continue
			}

			coords, err := NewCoords(uint8(i), uint8(j))
			if err != nil {
				return nil, err
			}

			if !m.Valid(digit, coords) {
				return nil, fmt.Errorf("invalid digit: %v at coords: (%v, %v)", digit, coords.X, coords.Y)
			}

			m.Rows[i][j] = digit
			m.Cols[j][i] = digit
		}
	}

	if clues < 17 {
		return nil, errors.New("sudoku must have at least 17 clues")
	}

	return m, nil
}

//NewCoords accepts two integers and outputs the new coords.
func NewCoords(x, y uint8) (*Coords, error) {
	if x > 8 {
		return nil, fmt.Errorf("invalid X coords: %v > 8", x)
	}

	if y > 8 {
		return nil, fmt.Errorf("invalid Y coords: %v > 8", y)
	}

	return &Coords{
		X: x,
		Y: y,
	}, nil
}

//Valid checks if the given digit and coords are valid with the respect to the
//provided matrix.
func (m *Matrix) Valid(digit uint8, coords *Coords) bool {
	//Valid is called every time and both errors.New and
	//fmt.Errorf are slow, one can see it when doing profiling

	if digit > 9 {
		return false
	}

	//Check the row.
	for _, cell := range m.Rows[coords.X] {
		if cell == digit {
			return false
		}
	}

	//Check the column.
	for _, cell := range m.Cols[coords.Y] {
		if cell == digit {
			return false
		}
	}

	//Check the square.
	square := m.LocalizeSquare(coords)
	for _, row := range square.Rows {
		for i := range row {
			if row[i] == digit {
				return false
			}
		}
	}

	return true
}

//LocalizeSquare finds the specific square based on the given coords.
func (m *Matrix) LocalizeSquare(coords *Coords) *Square {
	//Find the starting points of the square
	h := coords.X / 3 * 3
	v := coords.Y / 3 * 3

	return &Square{
		Rows: [3][3]uint8{
			{m.Rows[h][v], m.Rows[h][v+1], m.Rows[h][v+2]},
			{m.Rows[h+1][v], m.Rows[h+1][v+1], m.Rows[h+1][v+2]},
			{m.Rows[h+2][v], m.Rows[h+2][v+1], m.Rows[h+2][v+2]},
		},
		Cols: [3][3]uint8{
			{m.Cols[h][v], m.Cols[h][v+1], m.Cols[h][v+2]},
			{m.Cols[h+1][v], m.Cols[h+1][v+1], m.Cols[h+1][v+2]},
			{m.Cols[h+2][v], m.Cols[h+2][v+1], m.Cols[h+2][v+2]},
		},
	}
}

//Hash calculates the SHA256 hash of the matrix.
func (m *Matrix) Hash() (string, error) {
	hash := sha256.New()
	_, err := hash.Write([]byte(fmt.Sprintf("%v", *m)))
	if err != nil {
		return "", err
	}

	return hex.EncodeToString(hash.Sum(nil)), nil
}

//Copy copies the matrix passed as the receiver into the matrix passed as the argument.
func (m *Matrix) Copy() *Matrix {
	matrix := &Matrix{}

	for i, row := range m.Rows {
		for j, cell := range row {
			matrix.Rows[i][j] = cell
		}
	}

	for i, col := range m.Cols {
		for j, cell := range col {
			matrix.Cols[i][j] = cell
		}
	}

	return matrix
}

//Transpose transposes the matrix passed as the receiver into the matrix passed as the argument.
func (m *Matrix) Transpose() *Matrix {
	matrix := &Matrix{}

	for i, row := range m.Rows {
		for j, cell := range row {
			matrix.Cols[i][j] = cell
		}
	}

	for i, col := range m.Cols {
		for j, cell := range col {
			matrix.Rows[i][j] = cell
		}
	}

	return matrix
}

//Solved checks if the matrix has already been solved.
func (m *Matrix) Solved() bool {
	for i := uint8(0); i < 9; i++ {
		for j := uint8(0); j < 9; j++ {
			if m.Rows[i][j] != 0 {
				continue
			}

			return false
		}
	}

	return true
}

//Solve performs a brute-force recursion to find all solutions of the matrix.
func (m *Matrix) Solve() (*Matrix, error) {
	//Copy the matrix, so the original stays untouched.
	matrix := m.Copy()

	if !matrix.solve() {
		return nil, errors.New("no solutions found")
	}

	return matrix, nil
}

//solve is called recursively to find the solution using brute-force.
func (m *Matrix) solve() bool {
	if m.Solved() {
		return true
	}

	for i := uint8(0); i < 9; i++ {
		for j := uint8(0); j < 9; j++ {
			if m.Rows[i][j] != 0 {
				continue
			}

			coords, err := NewCoords(i, j)
			if err != nil {
				return false
			}

			for digit := uint8(1); digit < 10; digit++ {
				//False means that the current digit
				//produces an invalid matrix.
				if !m.Valid(digit, coords) {
					continue
				}

				//Assume some values.
				m.Rows[i][j] = digit
				m.Cols[j][i] = digit

				if m.solve() {
					return true
				}

				//Reset the values.
				m.Rows[i][j] = 0
				m.Cols[j][i] = 0
			}

			return false
		}
	}

	return false
}

//String outputs the matrix in an easy-to-read format.
func (m *Matrix) String() string {
	var output string

	output += "\n"
	output += "+---+---+---+\n"

	for i, row := range m.Rows {
		output += "|"
		for j, digit := range row {
			if digit == 0 {
				output += " "
			} else {
				output += strconv.Itoa(int(digit))
			}
			if (j+1)%3 == 0 {
				output += "|"
			}
		}
		output += "\n"
		if (i+1)%3 == 0 {
			output += "+---+---+---+\n"
		}
	}

	return output
}

//String outputs the square in an easy-to-read format.
func (s *Square) String() string {
	var output string

	output += "\n"
	output += "+---+\n"

	for i, row := range s.Rows {
		output += "|"
		for j, digit := range row {
			if digit == 0 {
				output += " "
			} else {
				output += strconv.Itoa(int(digit))
			}
			if (j+1)%3 == 0 {
				output += "|"
			}
		}
		output += "\n"
		if (i+1)%3 == 0 {
			output += "+---+\n"
		}
	}

	return output
}
